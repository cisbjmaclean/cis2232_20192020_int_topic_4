package info.hccis.teetimebooker.controllers;

import info.hccis.teetimebooker.util.CisUtility;
import info.hccis.teetimebooker.util.UtilityRest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    @RequestMapping("/")
    public String home(HttpSession session, Model model) {

        //BJM 20200602 Issue#1 Set the current date in the session
        String currentDate = CisUtility.getCurrentDate("yyyy-MM-dd");
        session.setAttribute("currentDate", currentDate);

        //Access the weather service
        final String WEATHER_REST_URL = "http://apidev.accuweather.com/currentconditions/v1/1327.json?language=en&apikey=hoArfRosT1215";
        String weatherDescription = UtilityRest.getJsonFromRest(WEATHER_REST_URL);

        System.out.println("BJTEST, json returned=" + weatherDescription);

        JSONArray jsonArray = new JSONArray(weatherDescription);
        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
        JSONObject temperature = jsonObject1.getJSONObject("Temperature");
        JSONObject metric = temperature.getJSONObject("Metric");
        float currentTemperature = metric.getFloat("Value");

        System.out.println("In charlottetown outside it is " + currentTemperature + "C outside");

//        If the temperature is > 25 tell the user to bring some extra water
//If the current temperature is <10 tell the user to bring their coats
//Otherwise say it is a nice
//        
        String message;
        if (currentTemperature > 25) {
            message = "bring some extra water";
        } else if (currentTemperature < 10) {
            message = "bring your coat";
        } else {
            message = "it is a nice day";
        }
        model.addAttribute("weatherMessage", message);

        return "index";
    }

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
